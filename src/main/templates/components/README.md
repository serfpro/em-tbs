### main/templates/components

Place components here that _only_ need a template. More complicated components
need to specify their templates by name anyway.

Seems to have something to do with how we're registering them...
