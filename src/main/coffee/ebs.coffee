###
 # EM-TBS: /src/main/coffee/ebs.coffee
 #
 # Copyright 2013-2015 Serf Productions, LLC
 # Licensed under MIT (/LICENSE)
###

# Defines the EBS Namespace
window.EBS = Em.Namespace.create
  VERSION: '0.0.1'

  # A "Map" of components to register.
  components: Em.Object.create {}

  registerComponents: () ->
    for key in Object.keys(@components)
      Ember.Handlebars.helper(key, @components.get key)

# Import the library.
require '/ebs/index'

# Register the EM-TBS plugin with the Application.
Em.Application.initializer
  name: 'ebs'

  # Registers the EM-TBS Components on Application.init()
  initialize: () -> EBS.registerComponents()
