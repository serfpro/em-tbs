###
 # EM-TBS: /src/main/coffee/ebs/index.coffee
 #
 # Copyright 2013-2015 Serf Productions, LLC
 # Licensed under MIT (/LICENSE)
###

require '/ebs/components/index'
require '/ebs/views/index'
require '/ebs/routes/index'
