###
 # EM-TBS: /src/main/coffee/routes/edit.coffee
 #
 # Copyright 2013-2015 Serf Productions, LLC
 # Licensed under MIT (/LICENSE)
###

EBS.BaseEditRoute = Ember.Route.extend
  # A catch-all that rolls back the edited model
  deactivate: ->
    model = @get('currentModel')
    if !model.get('isDeleted') && model.get('isDirty')
      @get('currentModel').rollback()

  # Saves a record.
  handleSave: (model) ->
    model.save().then (saved) =>
      model.reload().then => @send 'exit'

  handleRemove: (model) ->
    asset  = @get 'assetName'
    name   = model.get 'name'
    delMsg = if name? then "#{asset}, \"#{name}\"" else asset

    return unless model.get('id') &&
      confirm("Really delete the #{delMsg}?")

    model.destroyRecord().then(
      (=>
        console.log 'YAY!'
        @send 'list'
      ), ((reason) =>
        if reason.status == 200
          console.log 'Received 200, Error was fake'
          @send 'list'
        else console.log reason
      )
    )

# Base Route for making 'New' Records
EBS.EditNewRoute = EBS.BaseEditRoute.extend
  # Use a basic, empty JSON object as the model for new records.
  model: -> @store.createRecord @get('modelType')

  # Sub-classes must define to produce new records.
  modelType: -> null

  actions:
    # Saves a new record.
    save: (model) -> @handleSave(model)

    # Transitions to the list view.
    exit: -> @send 'list'

# Base Route for editing exiting assets.
EBS.EditModifyRoute = EBS.BaseEditRoute.extend
  actions:
    # Updates an existing record.
    save: (model) -> @handleSave model

    # Attempts to delete an existing record.
    remove: (model) -> @handleRemove model

    # Transitions to the previous route.
    exit: -> @send 'show', @get('currentModel')
