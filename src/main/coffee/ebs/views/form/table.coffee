###
 # EM-TBS: /src/main/coffee/views/form/table.coffee
 #
 # Copyright 2013-2015 Serf Productions, LLC
 # Licensed under MIT (/LICENSE)
###

EBS.DefaultRowActionsView = Ember.View.extend
  superViewBinding: 'parentView.superView'

  templateName: 'ebs/form/defaultRowActions'
  classNames:   ['actionCell']
  tagName:      'td'

EBS.MarkdownCellView = Ember.View.extend
  templateName: 'ebs/components/md-content'
  tagName: 'td'

EBS.BootstrapTableView = Ember.View.extend
  tagName:    'table'
  classNames: ['table', 'table-condensed']

  template: Ember.Handlebars.compile (
    "<thead>{{view view.headerView}}</thead>{{view view.rowsView}}" )

  superViewBinding: 'parentView'

  # Table Headers
  headers: []

  # Rows of data
  rows: []

  # Used to fetch values by property
  properties: []

  # Lists MarkDown enabled properties.
  mdProps: []

  # Per-row actions to show.
  showActions: null

  headerView: Ember.CollectionView.extend
    headersBinding:     'parentView.headers'
    showActionsBinding: 'parentView.showActions'

    content: []
    tagName: 'tr'

    didInsertElement: () ->
      headers = @get('headers').toArray()
      headers.push '' if @get('showActions')
      @set 'content', headers

    itemViewClass: Ember.View.extend
      template: Ember.Handlebars.compile "{{view.content}}"
      tagName: 'th'

  rowsView: Ember.CollectionView.extend
    contentBinding:     'parentView.rows'
    propertiesBinding:  'parentView.properties'
    mdPropsBinding:     'parentView.mdProps'
    showActionsBinding: 'parentView.showActions'
    superViewBinding:   'parentView.superView'

    tagName: 'tbody'

    createChildView: (viewClass, attrs) ->
      attrs.row = attrs.content
      delete attrs.content

      @_super(viewClass, attrs)

    itemViewClass: Ember.CollectionView.extend
      propertiesBinding:  'parentView.properties'
      mdPropsBinding:     'parentView.mdProps'
      showActionsBinding: 'parentView.showActions'
      superViewBinding:   'parentView.superView'

      classNameBindings: ['canRead::hidden']

      content: []
      tagName: 'tr'

      canRead: (() ->
        (typeof @get('row.module') == 'undefined' || @get('row.canRead'))
      ).property 'row.module', 'row.canRead'

      didInsertElement: ->
        @updateContent()
        @get('properties').forEach (prop) =>
          @addObserver 'row.' + prop, @updateContent

      willDestroyElement: ->
        @get('properties').forEach (prop) =>
          @removeObserver 'row.' + prop

      # Extract data from the current row.
      updateContent: ->
        mdProps = @get 'mdProps'

        values = []
        @get('properties').forEach (prop) =>
          value = @get('row.' + prop)
          values.push if mdProps.contains prop
            EBS.MarkdownCellView.extend(content: value)
          else value

        showActions = @get('showActions')
        values.push showActions if showActions
        @set 'content', values

      createChildView: (viewClass, attrs) ->
        #if attrs.contentIndex is 0
        #  viewClass = @get('nowrapViewClass')
        #else

        if (attrs.content && attrs.content.create)
          viewClass = attrs.content
          delete attrs.content

        @_super(viewClass, attrs)

      # Displays the extracted data in table cells.
      itemViewClass: Ember.View.extend
        templateName: 'ebs/components/basic-content'
        tagName: 'td'

      nowrapViewClass: Ember.View.extend
        templateName: 'ebs/components/basic-content'
        attributeBindings: ['nowrap']
        nowrap: 'nowrap'
        tagName: 'td'
