###
 # EM-TBS: /src/main/coffee/views/form/itemList.coffee
 #
 # Copyright 2013-2015 Serf Productions, LLC
 # Licensed under MIT (/LICENSE)
###

require '/ebs/views/form/modalForm'
require '/ebs/views/form/table'

EBS.ItemListView = EBS.ControlGroupView.extend
  templateName: 'ebs/form/itemList'

  didInsertElement: ->
    @_super()

    prop = @get('content.property')
    modalForm = @createChildView(EBS.ModalFormView)
    #modalForm.set('elementId', prop + 'Modal')
    modalForm.set('formName',  prop + 'Form')
    modalForm.append()

    @set('modalForm', modalForm)

  willDestroyElement: ->
    @_super()
    @get('modalForm').remove()

  newItem: ->
    itemType = @get 'content.itemType'
    @get('controller.store').createRecord itemType

  handleEdit: (model) ->
      @set('saved', false)
      @set 'modalForm.model', model
      @get('modalForm').show()

  actions:
    addItem: ->
      @set('saved', false)
      @set('modalForm.model', @newItem())
      @get('modalForm').show()

    save: (model) ->
      @set('saved', true)
      @get('listItems').addRecord(model) #if model.get('isNew')
      @get('modalForm').hide()

    edit: (model) -> @handleEdit(model)

    remove: (model) ->
      return unless confirm("Really remove the #{@get 'content.typeLabel'}?")
      @get('listItems').removeObject model

    cancel: (model) ->
      return if @get('saved')
      model.rollback() if model && !model.get('isNew') && model.get('isDirty')
      @get('modalForm').hide()

  # Can't use a binding as they don't seem to get overriden.
  listItems: (() -> @get('recordValue')).property('recordValue')

  # Whether or not there are any data rows to display.
  hasContent: (() ->
    @get('listItems.length') > 0
  ).property('listItems.length')

  showCancel: (() ->
    model = @get('modalForm.model')
    if model
      !(model.get('isNew') && @get('listItems').contains model)
    else true
  ).property 'modalForm.model'

# Registers the 'itemList' component with the EditForm
EBS.componentTypes.set 'itemList', EBS.ItemListView

EBS.ItemListData = Ember.Object.extend
  kind: 'itemList'
  itemType: null
  typeLabel: ''
  property: ''
  label: ''
  edit: []
  header: []
  properties: []
  mdProps: []
