###
 # EM-TBS: /src/main/coffee/views/form/modalForm.coffee
 #
 # Copyright 2013-2015 Serf Productions, LLC
 # Licensed under MIT (/LICENSE)
###

# Generates the Form Components in a Model Dialog
EBS.ModalFormView = EBS.EditFormView.extend
  templateName: 'ebs/form/modalForm'
  classNames: ['modal', 'fade']
  formName: 'modalForm'

  didInsertElement: ->
    $(@get 'element').on 'hidden.bs.modal', => @send 'cancel'
  destroyElement: -> @hide => @_super()

  content: (() ->
    @get 'parentView.content'
  ).property 'parentView.content'

  components: (() ->
    @get 'content.edit'
  ).property 'content.edit'

  show: () ->
    $(@get 'element').on 'shown.bs.modal', () =>
      @$(':input[type="text"]')[0]?.focus()
    $(@get 'element').modal 'show'

    editors = @get('children.childViews').filter (item) ->
      typeof item.get('isEditing') != 'undefined'
    editors.map (editor) -> editor.set('isEditing', true)

  hide: (f) ->
    if f?
      $(@get 'element').bind('hidden.bs.modal', f).modal 'hide'
    else
      $(@get 'element').modal 'hide'

  actions:
    cancel: () ->
      @get('parentView').send('cancel', @get 'model')
    save: () ->
      @get('parentView').send('save', @get 'model')
