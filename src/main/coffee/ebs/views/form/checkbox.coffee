###
 # EM-TBS: /src/main/coffee/views/form/checkbox.coffee
 #
 # Copyright 2013-2015 Serf Productions, LLC
 # Licensed under MIT (/LICENSE)
###

EBS.CheckboxView = EBS.ControlGroupView.extend
  templateName: 'ebs/form/checkbox'

EBS.CheckboxData = EBS.CommonData.extend
  kind: 'checkbox'
