###
 # EM-TBS: /src/main/coffee/views/form/editor.coffee
 #
 # Copyright 2013-2015 Serf Productions, LLC
 # Licensed under MIT (/LICENSE)
###

marked.setOptions
  gfm:         true
  tables:      true
  breaks:      false
  sanitize:    true
  smartLists:  true
  smartyPants: true

Ember.Handlebars.helper 'marked', (value) ->
  new Handlebars.SafeString(marked(value)) if value

# Renders a MarkDown Editor, generally used for full-text descriptions.
EBS.EditorView = EBS.ControlGroupView.extend
  templateName: 'ebs/form/editor'
  isEditing: true

  actions:
    preview: () -> @set('isEditing', false)
    edit:    () -> @set('isEditing', true )

EBS.EditorData = EBS.CommonData.extend
  kind: 'editor'
  maxLength: 1000000
