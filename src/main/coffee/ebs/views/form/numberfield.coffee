###
 # EM-TBS: /src/main/coffee/views/form/numberfield.coffee
 #
 # Copyright 2013-2015 Serf Productions, LLC
 # Licensed under MIT (/LICENSE)
###

EBS.NumberfieldView = EBS.ControlGroupView.extend
  templateName: 'ebs/form/numberfield'

EBS.NumberfieldData = EBS.CommonData.extend
  kind: 'numberfield'
  type: 'number'
  step: 'any'
  min:  null
  max:  null
