###
 # EM-TBS: /src/main/coffee/views/form/editForm.coffee
 #
 # Copyright 2013-2015 Serf Productions, LLC
 # Licensed under MIT (/LICENSE)
###

EBS.componentTypes = Ember.Object.create
  simpleSelect: EBS.SimpleSelectView
  enumSelect:   EBS.EnumSelectView
  kvpSelect:    EBS.KvpSelectView
  textarea:     EBS.TextareaView
  textfield:    EBS.TextfieldView
  numberfield:  EBS.NumberfieldView
  checkbox:     EBS.CheckboxView
  editor:       EBS.EditorView

# Default form generator.
EBS.EditFormView = Ember.View.extend
  templateName: 'ebs/form/defaultForm'
  tagName: 'fieldset'
  legend: null

  # Collection of Form Components
  componentsView: Ember.CollectionView.extend
    componentTypes: EBS.componentTypes
    tagName: 'div'

    createChildView: (view, attrs) ->
      if attrs
        view = @get('componentTypes.' + attrs.content.kind)
        view = @unknownView unless view

      @_super(view, attrs)

    # Sets the template to use when there aren't any form components.
    emptyView: Ember.View.extend
      templateName: 'ebs/form/emptyForm'

    # Sets the template to use when an unregistered component is being used.
    unknownView: Ember.View.extend
      templateName: 'ebs/form/unknownView'
