###
 # EM-TBS: /src/main/coffee/views/form/enumSelect.coffee
 #
 # Copyright 2013-2015 Serf Productions, LLC
 # Licensed under MIT (/LICENSE)
###

# Key-Value-Pair Select View. Enables the selection objects.
EBS.EnumSelectView = EBS.ControlGroupView.extend
  templateName: 'ebs/form/enumSelect'

  # Making it easier to extend EnumSelectView.
  items: (() -> @get('content.items')).property('content.items')

  # The value to display when the selected value is uneditable.
  displayValue: (() ->
    @get "#{@recordProperty()}.label"
  ).property 'model', 'items.@each'

EBS.EnumSelectData = EBS.CommonData.extend
  kind:   'enumSelect'
  prompt: null
  items:  []
