###
 # EM-TBS: /src/main/coffee/views/form/textfield.coffee
 #
 # Copyright 2013-2015 Serf Productions, LLC
 # Licensed under MIT (/LICENSE)
###

Ember.TextSupport.reopen
  attributeBindings: [
    "required"
    "name"
    "type"
    "step"
    "min"
    "max"
    "maxLength"
    "disabled"
  ]

EBS.TextfieldView = EBS.ControlGroupView.extend
  templateName: 'ebs/form/textfield'

EBS.TextfieldData = EBS.CommonData.extend
  kind:      'textfield'
  type:      'text'
  maxLength: 35
