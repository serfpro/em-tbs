###
 # EM-TBS: /src/main/coffee/views/form/simpleSelect.coffee
 #
 # Copyright 2013-2015 Serf Productions, LLC
 # Licensed under MIT (/LICENSE)
###

Ember.Select.reopen
  attributeBindings: ["required", "name"]

EBS.SimpleSelectView = EBS.ControlGroupView.extend
  templateName: 'ebs/form/simpleSelect'

EBS.SimpleSelectData = EBS.CommonData.extend
  kind:   'simpleSelect'
  prompt: null # The default text when no item is selected.
  items:  []   # What goes in the Select field.
