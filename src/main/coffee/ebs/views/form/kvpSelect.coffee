###
 # EM-TBS: /src/main/coffee/views/form/kvpSelect.coffee
 #
 # Copyright 2013-2015 Serf Productions, LLC
 # Licensed under MIT (/LICENSE)
###

# Key-Value-Pair Select View. Enables the selection objects.
EBS.KvpSelectView = EBS.ControlGroupView.extend
  templateName: 'ebs/form/kvpSelect'

  # Making it easier to extend KvpSelectView.
  items: (() -> @get('content.items')).property('content.items')

  # The value to display when the selected value is uneditable.
  displayValue: (() ->
    value = if @get('content.property')
      @get "#{@recordProperty()}"
    else @get('model')

    item = @findItem @get('optionValue'), value

    if item
      if item.get
        item.get(@get('optionLabel'))
      else
        item[@get('optionLabel')]
  ).property 'model', 'items.@each'

  # Manages the getting and setting of the record's value.
  # Overrides 'EBS.ControlGroupView.recordValue'
  recordValue: ((key, value) ->
    if @get 'content.property'
      recProp = @recordProperty()
      @setRecordValue(recProp, value) if (arguments.length != 1)
      @get recProp
    else
      @setRecordValue('model', value) if (arguments.length != 1)
      @get 'model'
  ).property 'model'

  # Controller's version of "optionValue"
  optionValue: ( ->
    @trimContent @get('content.optionValue')
  ).property 'content.optionValue'

  # Controller's version of "optionLabel"
  optionLabel: ( ->
    @trimContent @get('content.optionLabel')
  ).property 'content.optionLabel'

  # Sets the record's property to the given value.
  setRecordValue: (recProp, value) ->
    if value
      value = @findItem @get('optionValue'), value

      if value && !@get('content.retObj')
        value = value[@get 'optionValue']

    @set recProp, value

  # Trims "content." from the property name.
  trimContent: (propName) -> propName.split('.')[1]

  # Finds the item whose property value matches the one given.
  findItem: (optValue, value) ->
    if @get('content.retObj') && value.get
      value
    else
      @get('items').findProperty(optValue, value)

EBS.KvpSelectData = EBS.CommonData.extend
  kind: 'kvpSelect'

  optionValue: 'content.itemValue'
  optionLabel: 'content.itemLabel'
  retObj:      false
  prompt:      null
  items:       []
