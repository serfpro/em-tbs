###
 # EM-TBS: /src/main/coffee/views/form/textarea.coffee
 #
 # Copyright 2013-2015 Serf Productions, LLC
 # Licensed under MIT (/LICENSE)
###

EBS.TextareaView = EBS.ControlGroupView.extend
  templateName: 'ebs/form/textarea'

EBS.TextareaData = EBS.CommonData.extend
  kind:      'textarea'
  maxLength: 10000
