###
 # EM-TBS: /src/main/coffee/views/form/controlGroup.coffee
 #
 # Copyright 2013-2015 Serf Productions, LLC
 # Licensed under MIT (/LICENSE)
###

# Fields that are common to most Bootstrap Controls
EBS.CommonData = Ember.Object.extend
  kind:        null  # The type of component to render.
  _final:      false # Once set, the value cannot be changed.
  container:   null  # Container element for "popovers".
  required:    false # Whether or not the property is required.
  label:       null  # The UI display label of the property.
  property:    null  # The property of the model to show/edit.
  placeholder: null  # Descriptive text shown when the value is empty.
  info:        null  # General info about the field.
  help:        null  # Any help text about the field.

# Base View for Bootstrap "control-group" components
EBS.ControlGroupView = Ember.View.extend
  classNameBindings: [':form-group', 'isError:has-error']
  modelBinding: 'parentView.model'
  layoutName: 'ebs/layouts/bigCol'

  tagName: 'div'

  # Property's error message.
  errorMessage: ''

  # Sets up a dynamic Observer for Error Messages.
  didInsertElement: ->
    @addObserver  @errorBindingKey(), @updateError
    @$('[data-toggle="popover"]').popover
      placement: 'left'
      container: @get('content.container')
      trigger:   'hover'

  willDestroyElement: -> @removeObserver @errorBindingKey()

  # Getter/Setter for the record's property value.
  recordValue: ((key, value) ->
    key = @recordProperty()

    retVal = if (arguments.length == 1)
      @get(key)
    else
      @set(key, value)
      value

    if retVal == 0 then "0" else retVal
  ).property('model')

  # CSS class name binding utility.
  isError: (() ->
    if @get('errorMessage') then true else false
  ).property 'errorMessage'

  # Whether or not property can be changed. "Final" properties cannot be
  # changed once set.
  canUpdate: (() ->
    return true unless @get 'model.id'
    return false if @get('recordValue') && @get('content._final')
    return true;
  ).property 'model.id', 'recordValue', 'content._final'

  # Updates the property's error message.
  updateError: ->
    errMsgProp = @errorProperty() + '.0'
    @set 'errorMessage', @get(errMsgProp)

  # Constructs the key to find the property's value.
  recordProperty: -> 'model.' + @get('content.property')

  # Constructs the key to find the property's error message.
  errorProperty: -> 'model.errors.' + @get('content.property')

  # Constructs the observer binding for error messages.
  errorBindingKey: -> @errorProperty() + '.@each'
