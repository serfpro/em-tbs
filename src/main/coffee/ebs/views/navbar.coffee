###
 # EM-TBS: /src/main/coffee/views/navbar.coffee
 #
 # Copyright 2013-2015 Serf Productions, LLC
 # Licensed under MIT (/LICENSE)
###

### Nav Specific Mixins ###

# Generic, visual Nav properties.
EBS.NavViewMixin = Ember.Mixin.create
  classNames: ['nav', 'navbar-nav']
  tagName: 'ul'

### Views ###

# Provides a layout for rendering a Bootstrap Navbar.
EBS.NavbarView = Ember.View.extend
  layoutName: 'ebs/navbar'
  tagName: 'nav'
  classNames: ['navbar']
  attributeBindings: ['role']
  role: 'navigation'

  # Renders a simple Nav layout.
  NavView: Ember.View.extend EBS.NavViewMixin,
    icon: 'icon-check-empty'
    label: 'Tab'

  # Renders a collection of Navs.
  NavGroupView: Ember.View.extend EBS.NavViewMixin,
    templateName: 'ebs/navGroup'
    navItems: []
