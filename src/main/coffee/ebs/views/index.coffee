###
 # EM-TBS: /src/main/coffee/views/index.coffee
 #
 # Copyright 2013-2015 Serf Productions, LLC
 # Licensed under MIT (/LICENSE)
###

require '/ebs/views/navbar'
require '/ebs/views/form/index'
