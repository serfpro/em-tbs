###
 # EM-TBS: /src/main/coffee/components/nav-item.coffee
 #
 # Copyright 2013-2015 Serf Productions, LLC
 # Licensed under MIT (/LICENSE)
###

# Gives the component the ability to render the href attribute when needed.
EBS.NavItemComponent = Em.Component.extend
  layoutName: 'components/ebs-nav-item'
  attributeBindings: ['href']
  href: null

EBS.EbsActionNavComponent = Em.Component.extend
  layoutName: 'components/ebs-action-nav'
  actions:
    click: () -> @sendAction()

EBS.components.set('ebs-nav-item',   EBS.NavItemComponent)
EBS.components.set('ebs-action-nav', EBS.EbsActionNavComponent)
