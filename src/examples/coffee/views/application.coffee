###
 # EM-TBS: /src/examples/coffee/views/application.coffee
 #
 # Copyright 2013-2015 Serf Productions, LLC
 # Licensed under MIT (/LICENSE)
###

Ex.ApplicationView = Em.View.extend
  mainNavs: [
    {label: 'Home', icon: 'icon-home', route: 'index'}
    {label: 'Meow', icon: 'icon-user', route: 'meow' }
  ]
