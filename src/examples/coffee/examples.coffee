###
 # EM-TBS: /src/examples/coffee/examples.coffee
 #
 # Copyright 2013-2015 Serf Productions, LLC
 # Licensed under MIT (/LICENSE)
###

require '/ebs'
require '/app'
require '/views/index'
require '/routes/index'
