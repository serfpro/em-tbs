# EM-TBS

Welcome to EM-TBS (Ember - Twitter Bootstrap). Here, you'll find a handful of
utilities for generating Twitter Bootstrap styled forms and other components
using Ember.js.

While this code has been used in production for
[Savage Outfitter](https://savageoutfitter.com) for a few years now, the
libraries it depends on are very out of date. As such, it doesn't really mesh
well with the new ways of doing things in EmberJS.

This code is being Open Sourced in the hopes that it could prove useful to
someone, some how.

At _Serf Productions_, we may or may not one day get this library up to date
in the future; depending on how experiments with other technologies go.

Have fun!
