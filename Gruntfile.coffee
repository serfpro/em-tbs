###
 # EM-TBS Gruntfile
 # https://bitbucket.org/serfpro/em-tbs
 # Copyright 2013-2015 Twitter, Inc.
 # Licensed under MIT (/LICENSE)
###

'use strict'
path = require 'path'

folderMount = (connect, point) ->
  connect.static(path.resolve(point))

module.exports = (grunt) ->
  grunt.initConfig
    clean: { all: ['target', 'public/dist'] }

    server:
      port: 9495
      base: 'public'

    coffee:
      options: { bare: true }

      main:
        expand: true
        cwd:    'src/main/coffee'
        src:    ['**/*.coffee']
        dest:   'target/main'
        ext:    '.js'

      examples:
        expand: true
        cwd:  'src/examples/coffee'
        src:  ['**/*.coffee']
        dest: 'target/examples'
        ext:  '.js'

    commonjs:
      main:
        cwd:  'target/main/'
        src:  '**/*.js'
        dest: 'target/main/'

      examples:
        cwd:  'target/examples/'
        src:  '**/*.js'
        dest: 'target/examples/'

    emberTemplates:
      compile:
        options:
          templateName: (sourceFile) ->
            firstPass = sourceFile.replace(/src\/main\/templates\//, '')
            firstPass.replace(/src\/examples\/templates\//, '')
        files:
          'target/templates/main.js': ['src/main/templates/**/*.hbs']
          'target/templates/examples.js': ['src/examples/templates/**/*.hbs']

    concat:
      # Concats all the 3rd party JavaScript.
      deps:
        src: [
          'lib/common.js'
          'lib/moment.js'
          'lib/marked.js'
          'lib/jquery.js'
          'lib/bootstrap.js'
          'lib/handlebars.js'
          'lib/ember.js'
        ]
        dest: 'public/dist/deps.js'

      # Concats our eb-tbs sources into one file.
      main:
        src: [
          'target/templates/main.js'
          'target/main/**/*.js'
        ]
        dest: 'public/dist/em-tbs.js'

      # Concats our eb-tbs sources into one file.
      examples:
        src: [
          'target/templates/examples.js'
          'target/examples/**/*.js'
        ]
        dest: 'public/dist/examples.js'

    uglify:
      dist:
        files:
          'target/em-tbs.min.js': ['public/dist/em-tbs.js']

    watch:
      options:
        interrupt: true
        livereload: 35728
      public:
        files: ['public/**/*.html','public/**/*.css']
        tasks: []
      main:
        files: 'src/main/coffee/**/*.coffee'
        tasks: ['compile']
      examples:
        files: 'src/examples/coffee/**/*.coffee'
        tasks: ['compile']
      lib:
        files: 'lib/**/*.js'
        tasks: ['compile']
      main_templates:
        files: 'src/main/templates/**/*.hbs'
        tasks: ['compile']
      examples_templates:
        files: 'src/examples/templates/**/*.hbs'
        tasks: ['compile']

  grunt.loadNpmTasks 'grunt-coffee-server'
  grunt.loadNpmTasks 'grunt-commonjs'
  grunt.loadNpmTasks 'grunt-contrib-clean'
  grunt.loadNpmTasks 'grunt-contrib-coffee'
  grunt.loadNpmTasks 'grunt-contrib-concat'
  grunt.loadNpmTasks 'grunt-contrib-uglify'
  grunt.loadNpmTasks 'grunt-contrib-watch'
  grunt.loadNpmTasks 'grunt-ember-templates'

  grunt.registerTask 'compile', [
    'clean'
    'coffee'
    'commonjs'
    'emberTemplates'
    'concat'
  ]
  grunt.registerTask 'default', ['compile']
  grunt.registerTask 'monitor', ['compile', 'watch']
  grunt.registerTask 'dist',    ['compile', 'uglify']
